@echo off & setlocal

:: use strawberryperl openssl (dunno why)
:: pass filename to this script to generate solved files
:: fucking cmd dirtyhacks

if "%1"=="" (
    echo     usage: solve [ciphertext_filename]
    exit /b
)

:: fillin your own keys
set rc2=openssl rc2 -d -k 4f0c6a3e90401fa7bc51a5ae9982845c -in %1
set cast5cfb=openssl cast5-cfb -d -k 6fbdbcb790138cc054556b45dfaff777 -in %1
set aes192ecb=openssl aes-192-ecb -d -k 4befaa7b927a349af5d9f7c321419c8c -in %1
set desx=openssl desx -d -k 296c5b3207bf00fa3271287447b5e53a -in %1

set desecb=openssl des-ecb  -d -k acc6cccd9ee0df92e1f329151ac28a9d -in %1
set des3=openssl des3 -d -k 5324a480df9a59e444f4b82aa1e29b30 -in %1
set aes128cbc=openssl aes-128-cbc -d -k e11f3ce8443d192bb376d1d025de2540 -in %1
set camellia128=openssl camellia128 -d -k 617223fdcaf2532a633b2dc0675b9663 -in %1

set seed=openssl seed -d -k e1930b4927e6b6d92d120c7c1bba3421 -in %1
set aes256cbc=openssl aes-256-cbc -d -k 146983842e420321011e74145a59aa48 -in %1
set bf=openssl bf -d -k ae9d5595593d62eefa1687551497db39 -in %1
set desede=openssl des-ede -d -k 5f6b93462201020193c8664c600e3faa -in %1

set aes256=openssl aes256 -d -k df2240f7fd137880c89f375a81728256 -in %1



%rc2% 2>&1 1>nul | findstr "bad decrypt" > nul || (
    %rc2% -out %1.rc2 2>nul
    echo decrypted as rc2 ^(%1.rc2^)
)


%cast5cfb% 2>&1 1>nul | findstr "bad decrypt" > nul || (
    %cast5cfb% -out %1.cast5cfb 2>nul
    echo decrypted as cast5cfb ^(%1.cast5cfb^)
)


%aes192ecb% 2>&1 1>nul | findstr "bad decrypt" > nul || (
    %aes192ecb% -out %1.aes192ecb 2>nul
    echo decrypted as aes192ecb ^(%1.aes192ecb^)
)


%desx% 2>&1 1>nul | findstr "bad decrypt" > nul || (
    %desx% -out %1.desx 2>nul
    echo decrypted as desx ^(%1.desx^)
)

%desecb% 2>&1 1>nul | findstr "bad decrypt" > nul || (
    %desecb% -out %1.desecb 2>nul
    echo decrypted as desecb ^(%1.desecb^)
)


%des3% 2>&1 1>nul | findstr "bad decrypt" > nul || (
    %des3% -out %1.des3 2>nul
    echo decrypted as des3 ^(%1.des3^)
)


%aes128cbc% 2>&1 1>nul | findstr "bad decrypt" > nul || (
    %aes128cbc% -out %1.aes128cbc 2>nul
    echo decrypted as aes128cbc ^(%1.aes128cbc^)
)


%camellia128% 2>&1 1>nul | findstr "bad decrypt" > nul || (
    %camellia128% -out %1.camellia128 2>nul
    echo decrypted as camellia128 ^(%1.camellia128^)
)

%seed% 2>&1 1>nul | findstr "bad decrypt" > nul || (
    %seed% -out %1.seed 2>nul
    echo decrypted as seed ^(%1.seed^)
)

%aes256cbc% 2>&1 1>nul | findstr "bad decrypt" > nul || (
    %aes256cbc% -out %1.aes256cbc 2>nul
    echo decrypted as aes256cbc ^(%1.aes256cbc^)
)


%bf% 2>&1 1>nul | findstr "bad decrypt" > nul || (
    %bf% -out %1.bf 2>nul
    echo decrypted as bf ^(%1.bf^)
)


%desede% 2>&1 1>nul | findstr "bad decrypt" > nul || (
    %desede% -out %1.desede 2>nul
    echo decrypted as desede ^(%1.desede^)
)


%aes256% 2>&1 1>nul | findstr "bad decrypt" > nul || (
    %aes256% -out %1.aes256 2>nul
    echo decrypted as aes256 ^(%1.aes256^)
)

endlocal

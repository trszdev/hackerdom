import itertools, hashlib

sha256 = lambda x: hashlib.sha256(x).hexdigest()

alphabet = '0123456789abcdef'
key = list('88d2?eb2b+0ce3d30c6c7f*c93d9&b57')
digest = 'd5c9e1c7f862d634a759a56ed97549f62a6d78401440a5c09cfbf7dfc20e8190'
for x in itertools.product(alphabet, repeat=4):
    key[4] = x[0]
    key[9] = x[1]
    key[22] = x[2]
    key[28] = x[3]
    realkey = ''.join(key)
    if sha256(sha256(realkey))==digest:
            print realkey

import hashlib, itertools, sys

md5 = lambda x: hashlib.md5(x).hexdigest()
hashes = [x.replace('\n','') for x in open('hashes', 'r').readlines()]
cracked = {}
total = 100000
for x in xrange(0, total):
    print '\r\t%d/%d' % (x+1, total),
    q = bin(x)[2:]
    qmd5 = md5(q)
    if qmd5 in hashes: 
       cracked[qmd5] = q 
       print '\n', qmd5, q, len(cracked)

print 
print len(cracked), len(hashes)

message = [cracked[x] if x in cracked else '' for x in hashes]
message = ''.join(message)
message = [chr(int(message[x:x+8],2)) for x in xrange(0, len(message),8)]
print message

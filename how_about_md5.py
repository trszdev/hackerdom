# windows only! python2
import hashlib, ctypes

msvcr90_handle = ctypes.CDLL('msvcr90.dll')
rand = msvcr90_handle['rand']
srand = msvcr90_handle['srand']

def get_password(login):
    srand(get_seed(login))
    return ''.join(str(rand() % 10) for x in xrange(10))

def get_seed(login):
    mods = [0, 0xfb, 0xf1, 0xef, 0xe9, 0x53, 0x25, 0x17, 0xd, 0xb, 0x7]
    n = len(login)
    seed = 0
    mod = 7 if n>10 else mods[n]
    for x in xrange(n-1, -1, -1):
        seed*=mod
        seed+=(ord(login[x]) - 48) % mod
    return seed

def get_names():
    with open('how_about_md5_names', 'r') as f:
         return f.read().replace('\n', '').split()

if __name__=='__main__':
    names = get_names()
    assert len(names)==200
    md5 = lambda x: hashlib.md5(x).hexdigest()
    passwords = map(get_password, names)
    key = ''.join(passwords)
    print 'Example login, password: %s, %s' % (names[0], passwords[0])
    print 'Answer: %s' % md5(key)

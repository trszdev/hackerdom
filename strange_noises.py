import wave, struct

waveFile = wave.open('noise.wav', 'r')
length = waveFile.getnframes()
print 'Frames: %d, channels: %d' % (length, waveFile.getnchannels())

code = ''
last = 0
count = 0
for i in xrange(length):
    waveData = waveFile.readframes(1)
    data = struct.unpack("<h", waveData)[0]
    if data*last < 0:
        code += '.' if count > 3 else '-'
        count = 0
    last = data
    count += 1

print code


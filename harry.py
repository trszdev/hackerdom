import wave, struct

waveFile = wave.open('harry.wav', 'r')
length = waveFile.getnframes()
print 'Frames: %d, channels: %d' % (length, waveFile.getnchannels())


bits = []
for i in xrange(length):
    waveData = waveFile.readframes(1)
    a, b = waveData[:2], waveData[2:]
    a = struct.unpack("<h", a)[0]
    b = struct.unpack("<h", b)[0]
    bits.append(abs(a-b))


bytes = []
for x in xrange(0, len(bits), 8):
    byte = ''.join('1' if y else '0' for y in bits[x:x+8])
    bytes.append(int(byte, 2))

with open('out.wav', 'wb') as f:
    f.write(''.join(map(chr, bytes)))
    print 'saved to out.wav'


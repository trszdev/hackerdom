#!env python2
import socket,sys

def netcat(host, port, data):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((host, port))
    s.sendall(data)
    s.shutdown(socket.SHUT_WR)
    while 1: 
        piece1024 = s.recv(1024)
        if not piece1024: break
        yield piece1024
    s.close()


def netcat_batch(hostname, port, content):
    port = int(port)
    print 'Connecting to %s:%d' % (hostname, port)
    for piece1024 in netcat(hostname, port, content):
        print 'Received:', repr(piece1024)
    print "Connection closed"

if __name__=='__main__':
    if len(sys.argv) == 4: netcat_batch(*sys.argv[1:])
    else: print '\tusage: python nc.py [host] [port] [data]'

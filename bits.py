#!/usr/bin/env python2

def bits_encode(mask, message):
    result = []
    mask_byte = _get_message_byte(mask)
    tmp = message[:]
    for i in xrange(8):
        result.append(_get_message_byte(tmp)^mask_byte)
        tmp = tmp[1:] + [tmp[0]] #rot left
    return result

def _get_message_byte(msg):
    mbyte = 0
    for x in xrange(8): mbyte |= (1<<x) & msg[x]
    return mbyte

def bits_decode(mask, message):
    result = []
    mask_byte = _get_message_byte(mask)
    tmp = [message[x] for x in [0, 7, 6, 5, 4, 3, 2, 1]] #copy in 'reverse' order 
    for i in xrange(8):
        result.append(_get_message_byte(tmp)^mask_byte)
        tmp = [tmp[-1]] + tmp[:-1] #rot right
    return result


def main(mask, message):
    print "Message: %s" % str(message)
    print ''.join(map(chr, message)), '\n', '='*32
    encoded = bits_encode(mask, message)
    decoded = bits_decode(mask, message)
    print 'Encoded: %s' % str(encoded)
    print ''.join(map(chr, encoded)), '\n'
    print 'Decoded: %s' % str(decoded)
    print ''.join(map(chr, decoded)) 

if __name__=='__main__':
    mask = [0x69, 0xe7, 0x56, 0x5e, 0xb9, 0x82, 0xf, 0x4d]
    msg1 = map(ord, 'abcdefgh')
    msg2 = [124, 121, 122, 123, 112, 125, 126, 127]
    msg3 = map(int, '107 44 125 92 105 62 110 104'.split())
    main(mask, msg3)

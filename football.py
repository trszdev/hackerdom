#!env python2
def strtr(string, replaceMap):
    res = ''
    for x in string:
        res += replaceMap[x] if x in replaceMap else x
    return res

def makeReplaceMap(a1,a2):
    return dict(zip(a1,a2))

import codecs, itertools, math
text = codecs.open('football', 'r', 'utf8').read()[:-1]
print text

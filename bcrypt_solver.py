#!env python2
import bcrypt, base64 
from nc import netcat
pack = lambda a: a.decode('hex')

BCRYPT_COST = 8
BCRYPT_SALT = pack('aad28123bb4983bbb6749ee462cb0e10')
PASSWORD_LEN = len('****************')
HOST = 'bcrypt.training.hackerdom.ru'
PORT = 60001
BCRYPT_TRUNC_SIZE = 72


def main():
    salt = '$2b$%02d$%s' % (BCRYPT_COST, _b64_encode(BCRYPT_SALT))
    print 'PASSWORD_LEN: %d' % PASSWORD_LEN
    result = ''
    for i in xrange(1,PASSWORD_LEN+1):
        pepper = 'a'*(BCRYPT_TRUNC_SIZE - i - len(result)) + result
        pepper2 = ''.join(format(ord(x), '#04x')[2:] for x in pepper)
        hash = ''.join(netcat(HOST, PORT, pepper2))[:-1]
        next_letter = get_letter(pepper, hash, salt, result)
        result += next_letter
        print result, '\r',
    print


def get_letter(pepper, hash, salt, result):
    hash = _b64_encode(pack(hash))
    for i in map(chr, range(1, 256)):
        new_hash = bcrypt.hashpw(pepper+result+i, salt)[-31:]
        if new_hash == hash: 
            return i
    raise Exception('cannot find any letters for these ones: %s'
                    % [pepper, hash, salt]) 



# wont even remind you the author of next lines
def _b64_encode(data):
    import string
    B64_CHARS = ''.join((string.ascii_uppercase, string.ascii_lowercase,
                     string.digits, '+/'))
    B64_CHARS_BCRYPT = ''.join(('./', string.ascii_uppercase,
                            string.ascii_lowercase, string.digits))
    B64_TO_BCRYPT = string.maketrans(B64_CHARS, B64_CHARS_BCRYPT)
    B64_FROM_BCRYPT = string.maketrans(B64_CHARS_BCRYPT, B64_CHARS)
    enc = base64.b64encode(data)
    return enc.translate(B64_TO_BCRYPT, '=')


main()

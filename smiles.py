#!env python2

# known from 'QCTF_' and prev. results
rose = 0x4
ski = 0x5
knife = 0x3
five = 0x1
face = 0x6
cloud = 0xf

female = 0x2
tube = 0x0
phone = 0x7
train = 0x9

from itertools import permutations
unknown = [0x8, 0xA, 0xB, 0xC, 0xD, 0xE]


for p in permutations(unknown):
    male, panda, letter, chair, book, dot = p
    message = [
        rose, rose, face, cloud, female, tube, phone, train, face, cloud, phone, ski, female, tube, face, male, # 1st row
        face, train, face, panda, face, ski, female, tube, phone, train, face, cloud, phone, ski, phone, female, # 2nd row
        female, tube, face, letter, face, ski, phone, phone, female, tube, face, chair, face, cloud, face, book, # 3rd
        face, ski, female, phone, phone, knife, female, tube, phone, tube, face, five, phone, knife, phone, knife, # 4th
        phone, phone, face, cloud, phone, female, face, rose, knife, dot, female, tube, ski, five, rose, knife, # 5th

        ski, rose, rose, face, ski, cloud, face, book, face, ski, phone, female, knife, knife, knife, knife, # 6th
        face, panda, face, male, face, ski, knife, knife , phone, phone, face, face, phone, tube, knife, knife,
        knife, phone, face, chair, phone, face, knife, cloud, # last
    ]

    codes = (message[x]*16+message[x+1] for x in xrange(0, len(message)-1, 2))
    print ''.join(map(chr, codes))


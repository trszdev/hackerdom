﻿#!env python2
import requests, hashlib, re
url = 'http://emdee.training.hackerdom.ru/'

def emdee(secret):
    req = requests.post(url, data={'secret':secret})
    h = map(lambda x: x[:32], re.findall('<em>([^<>]+\-)</em>', req.text))[1]
    t = re.findall('\d+\.\d+', req.text)[0]
    return h,t 

md5 = lambda x: hashlib.md5(x).hexdigest()
# from wiki:
# \x13 (device control 3) - temp. interrupts current input
# \x11 (device control 1) - allows to continue input
# \b - moves cursor back into 1 position
# \r - moves cursor to start 
# \x0D - cr
# \x0A - lf q   
# \xD1 - forward char 
# \x7F - del char

# we try to recompute hash with \x7f symbols, getting salt:
salt = 'a8REdj4#7fnFwqp2X98cksu@k^s2Bsjgi5FG23f!'

